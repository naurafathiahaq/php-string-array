<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller{
    public function register()
    {
        return view('register');
    }

    public function submit(Request $request)
    {
        $namaDepan = $request->input('first');
        $namaBelakang = $request ->input('last');
        return view('welcome',['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}